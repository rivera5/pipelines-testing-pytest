"""
Based on https://www.youtube.com/watch?v=r7Dtus7N4pI
"""
import time
from typing import Callable


def timeit(funct: Callable) -> None:
    def wrapper():
        before: float = time.time()
        funct()
        after: float = time.time()
        print(f"Function took {after - before} seconds to execute.")

    return wrapper


@timeit
def run():
    time.sleep(2)


run()
