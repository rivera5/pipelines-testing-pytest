"""
Taken from https://realpython.com/primer-on-python-decorators
"""
import functools
from typing import Callable, Any


def repeat(num_times: int) -> Any:
    def decorator_repeat(func: Callable) -> Any:
        @functools.wraps(func)
        def wrapper_repeat(*args, **kwargs) -> Any:
            for _ in range(num_times):
                value = func(*args, **kwargs)
            return value
        return wrapper_repeat
    return decorator_repeat


@repeat(num_times=4)
def greet(name):
    print(f"Hello {name}")


greet("Quantum Bootcamp Students!")
