"""
Based on https://www.youtube.com/watch?v=r7Dtus7N4pI
"""
import datetime
from typing import Callable, Any


def log(funct: Callable) -> Any:
    def wrapper(*args, **kwargs):
        function_name = getattr(funct, "__name__", repr(funct))
        with open("logs.txt", "a") as file:
            file.write(f"Called function {function_name} with {[str(arg) for arg in args ]} at "
                       f"{str(datetime.datetime.now())}\n")
            file.close()
        result: float = funct(*args, **kwargs)
        return result
    return wrapper


@log
def sum_floats(*args) -> float:
    result: float = 0
    for num in args:
        result += num
    return result


print(sum_floats(1, 2, 3, 4))
