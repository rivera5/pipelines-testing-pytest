class Circle:
    """
    Code taken from: https://realpython.com/primer-on-python-decorators/#timing-functions
    >>> c = Circle(5)
    >>> c.radius
    5
    >>> c.area
    78.5398163375
    >>> c.radius = 2
    >>> c.area
    12.566370614
    >>> c.area = 100
    Traceback (most recent call last):
    ...
    AttributeError: can't set attribute 'area'
    >>> c.cylinder_volume(height=4)
    50.265482456
    >>> c.radius = -1
    Traceback (most recent call last):
    ...
    ValueError: Radius must be positive
    >>> c = Circle.unit_circle()
    >>> c.radius
    1
    >>> c.pi()
    3.1415926535
    >>> Circle.pi()
    3.1415926535
    """
    def __init__(self, radius):
        self._radius = radius

    @property
    def radius(self):
        """Get value of radius"""
        return self._radius

    @radius.setter
    def radius(self, value):
        """Set radius, raise error if negative"""
        if value >= 0:
            self._radius = value
        else:
            raise ValueError("Radius must be positive")

    @property
    def area(self):
        """Calculate area inside circle"""
        return self.pi() * self.radius**2

    def cylinder_volume(self, height):
        """Calculate volume of cylinder with circle as base"""
        return self.area * height

    @classmethod
    def unit_circle(cls):
        """Factory method creating a circle with radius 1"""
        return cls(1)

    @staticmethod
    def pi():
        """Value of π, could use math.pi instead though"""
        return 3.1415926535
