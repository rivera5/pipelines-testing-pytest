from typing import Callable


def function_1() -> None:
    print("Called f1")


def function_2(function: Callable) -> Callable:
    def wrapper() -> None:
        print("Before calling the function.")
        function()
        print("After calling the function.")

    return wrapper


# Decorating the function_1 with the behaviour of function_2
function_1 = function_2(function_1)  # Uses function aliasing
# Calling the decorated function
function_1()
