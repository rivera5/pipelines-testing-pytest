from typing import Callable


def function_1() -> None:
    print("Called f1")


def function_2(function: Callable) -> None:
    function()


# Function treated as an object
funct: Callable = function_1
# Calling\invoking function_1 from function_2
function_2(function=funct)
# Printing out the function pointer
print(f"Pointer of function_1: {funct}")
