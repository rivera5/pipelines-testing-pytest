from typing import Callable


def function_2(function: Callable) -> Callable:
    def wrapper() -> None:
        print("Before calling the function.")
        function()
        print("After calling the function.")

    return wrapper


@function_2
def function_1() -> None:
    print("Called f1")


# Calling the decorated function function_1
function_1()
