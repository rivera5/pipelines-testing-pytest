from typing import Callable


def function_2(function: Callable) -> Callable:
    def wrapper(*args, **kwargs):
        print("Before calling the function.")
        result: int = function(*args, **kwargs)
        print("After calling the function.")
        return result

    return wrapper


@function_2
def function_1(a: int, b: int) -> int:
    return a + b


# Calling the decorated function function_1
print(function_1(a=10, b=20))
