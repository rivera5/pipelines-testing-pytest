from typing import Callable


def function_2(function: Callable) -> Callable:
    def wrapper(*args, **kwargs) -> None:
        print("Before calling the function.")
        function(*args, **kwargs)
        print("After calling the function.")

    return wrapper


@function_2
def function_1(text: str) -> None:
    print(f"Called function_1 and received the argument: {text}")


# Calling the decorated function function_1
function_1("Hola!")
