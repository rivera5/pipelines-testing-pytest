from typing import Callable


def function_1() -> None:
    print("Called f1")


def function_2(function: Callable) -> Callable:
    def wrapper() -> None:
        print("Before calling the function.")
        function()
        print("After calling the function.")
    return wrapper


# Function treated as an object
funct: Callable = function_1
# Calling\invoking function_1 from function_2 and getting the data type from the return
# Notice how wrapper is not called:
print(type(function_2(function=funct)))
# To call the wrapper() function we need:
function_2(function=funct)()
